#!/usr/bin/env perl

use strict;
use warnings;

use English qw( -no_match_vars );
use Log::Any::Adapter 'Stderr', log_level => 'info';
use Net::Stomp;

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialized.');

my $read_timeout  = 0;

my $stomp = Net::Stomp->new({
    hostname           => 'localhost',
    port               => 61613,
    logger             => Log::Any->get_logger(category => 'Net::Stomp'),
    reconnect_attempts => 3,
#    timeout            => $read_timeout,
});

my $connect_frame = $stomp->connect({
    login    => 'artemis',
    passcode => 'artemis',
});
unless ( defined $connect_frame ) {
    $log->error('Unable to connect.  Quitting.');

    exit 1;
}

$log->info("CONNECTION:\n" . $connect_frame->body);

$stomp->subscribe({
    destination => '/queue/demo_stomp',
    ack         => 'client',
});

my $quit = 0;

#$SIG{INT} = sub {
#    $log->warn('Caught INT signal.');
#};
#$SIG{TERM} = sub {
#    $log->warn('Caught TERM signal.');
#};

my $loop_wait = 1;

my $receive_timeout = 1;
while ( ! $quit ) {
    my $frame = $stomp->receive_frame({ timeout => $receive_timeout });
    unless ( defined $frame ) {
        next;
    }

    my $body = $frame->body;
    $log->info("BODY:\n" . $body);

    $stomp->ack({
        frame => $frame,
    });

    sleep $loop_wait;
}

$stomp->disconnect;

