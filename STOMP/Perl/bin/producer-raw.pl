#!/usr/bin/env perl

use strict;
use warnings;

use English qw( -no_match_vars );
use Log::Any::Adapter 'Stderr', log_level => 'info';
use Net::Stomp;

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialized.');

my $read_timeout  = 0;

my $stomp = Net::Stomp->new({
    hostname           => 'localhost',
    port               => 61613,
    logger             => Log::Any->get_logger(category => 'Net::Stomp'),
    reconnect_attempts => 3,
#    timeout            => $read_timeout,
});

my $connect_frame = $stomp->connect({
    login    => 'artemis',
    passcode => 'artemis',
});
unless ( defined $connect_frame ) {
    $log->error('Unable to connect.  Quitting.');

    exit 1;
}

$log->info("CONNECTION:\n" . $connect_frame->body);

$stomp->send({
    destination => '/queue/demo_stomp',
    body        => 'demo stomp message',
});

$stomp->disconnect;

