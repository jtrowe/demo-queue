= STOMP / Perl

Demonstration of using STOMP with Perl.


== Run consumer-raw.pl

WIll attempt to connect to the STOMP server hosted by the ActiveMQ
broker.

[source,sh]
-----------
carton exec -- perl consumer-raw.pl
# CTRL-C to exit wait loop
-----------


== Links and References

*   https://metacpan.org/pod/Net::Stomp
*   https://stomp.github.io/

