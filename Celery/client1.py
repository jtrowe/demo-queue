
from proj.celery import add

res = add.delay(4, 3)
x = res.get(timeout=4)
print(f"{x}".format(x=x))
