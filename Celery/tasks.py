from celery import Celery
import time

app = Celery('tasks', backend='rpc://', broker='pyamqp://guest:guest@localhost//')

@app.task
def add(x, y):
    return x + y

@app.task
def add_slow(x, y):
    time.sleep(7)
    return x + y

