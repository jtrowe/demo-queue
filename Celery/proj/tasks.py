from .celery import app

@app.task
def add(x, y):
    return x + y

@app.task
def test(arg):
    print("task[test]: arg => {}".format(arg))

