from celery import Celery
from celery.schedules import crontab

app = Celery()
app.config_from_object('celeryconfig')


# use this since tasks are defined elsewhere: see userguid / periodic tasks
#@app.on_after_finalize.connect

# Might not be aware of tasks loaded via include in the config file.

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(10.0, test2.s('hello'), name='add every 10')

    # Calls test('hello') every 30 seconds.
    # It uses the same signature of previous task, an explicit name is
    # defined to avoid this task replacing the previous one defined.
    sender.add_periodic_task(30.0, test2.s('hello'), name='add every 30')

    # Calls test('world') every 30 seconds
    sender.add_periodic_task(97.0, test2.s('world'), expires=10)

    # Executes every Friday morning at 4:25 p.m.
    sender.add_periodic_task(
        crontab(hour=16, minute=25, day_of_week=5),
        test2.s('Happy Friday!'),
    )


@app.task
def test2(arg):
    print("task[test2]: arg => {}".format(arg))


if __name__ == '__main__':
    app.start()
