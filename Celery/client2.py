from celery import Celery

app = Celery("tasks", backend="rpc://", broker="pyamqp://guest:guest@localhost//")

print("Calling tasks.add_slow")
res = app.send_task("tasks.add_slow", args=[2, 8], kwargs={})
x = res.get(timeout=10)
print(f"{x}".format(x=x))

# repr is how Python does dumps in the Python shell?
#i = app.control.inspect()
#print("registered: ", repr(i.registered()))

