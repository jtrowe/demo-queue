use strict;
use warnings;

use Gearman::Client;
use Storable qw( freeze );

my $client = Gearman::Client->new;
$client->job_servers('localhost');

my @input = @ARGV;
unless ( @input ) {
    @input = ( 1, 2 );
}
printf "input : %s\n", join(', ', @input);

my $result = $client->do_task('add', freeze(\@input));
printf "result : %s\n", ${ $result };

