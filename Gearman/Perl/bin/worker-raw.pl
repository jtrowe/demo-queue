use strict;
use warnings;

use lib '../../Perl-Common/lib';
use lib 'lib';

use DateTime::Format::ISO8601;
use Demo::Petstore::Report::InventoryReport;
use English;
use Gearman::Client;
use Gearman::Worker;
use JSON;
use Log::Any;
use Log::Any::Adapter 'Stderr';
use Try::Tiny;

my $j   = JSON->new->convert_blessed(1);
my $iso = DateTime::Format::ISO8601->new;

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialize.');

my $worker = Gearman::Worker->new;
my @servers = qw( localhost );
$worker->job_servers(@servers);

my $client = Gearman::Client->new;
$client->job_servers(@servers);

my $name;

$name = 'InventoryReport';
$worker->register_function(
    $name => build_shim(
        log  => $log,
        name => $name,
        sub  => sub {
            my %in = @_;

            my $dt    = $iso->parse_datetime(delete $in{date});
            $in{date} = $dt;

            my $g = Demo::Petstore::Report::InventoryReport->new(
                log => $log,
            );
            my $report = $g->generate(%in);
            return $report;
        },
    ),
);
$log->debug("Added task: $name");


$log->info('Waiting for jobs.');
for ( ;; ) {
    $worker->work(
    );
}


sub build_shim {
    my %param = @_;
    my $log   = $param{log};
    my $tname = $param{name};
    my $sub   = $param{sub};

    return sub {
        my $job = shift;

        my $arg = $job->arg // '{}';

        $log->debug("ENTERING: $tname: arg => $arg");

        my $result;
        try {
            my $raw_args = $j->decode($arg);
            my $raw_hash = $raw_args->[0];
            my $sleep    = $raw_hash->{sleep} // 1;
            if ( $sleep ) {
                sleep $sleep;
            }

            $log->debug("Creating job for task $tname.");
            $log->debug("Starting job for task $tname.");

            $log->debug("Starting job for task $tname. meta.inline => " .
                sprintf('%d', ( !! $raw_hash->{meta}->{inline} ))
            );

            my $report = &{ $sub }( %{ $raw_hash } );

            $log->debug("Encoding results for job $tname.");
            $result = $j->encode($report);

        }
        catch {
            $log->fatal("ERROR: job for task $tname: $_");
        };

        return $result;
    };
}

sub DateTime::TO_JSON {
    my $i = shift;
    return '' . $i;
}

