use strict;
use warnings;

use lib 'lib';

use Data::Dumper;
use English;
use Gearman::Client;
use Getopt::Long;
use JSON;
use Log::Any::Adapter 'Stderr';

my $j = JSON->new;

my $inline = 1;
GetOptions(
    'inline!' => \$inline,
);

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialize.');

my $client = Gearman::Client->new;
$client->job_servers('localhost');

my $task = shift @ARGV;

my $args;
unless ( @ARGV ) {
    $args  = { date => '2000-01-01' };
}

$log->debug("client-raw meta.inline => " .
    sprintf('%d', ( !! $inline ))
);
$args->{meta}->{inline} = $inline;

my $result = $client->do_task($task, $j->encode([ $args ]));
$log->info(sprintf "result: %s\n", Dumper($result));

