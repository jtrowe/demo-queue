
use strict;
use warnings;

use AnyEvent::Gearman::Worker;

my $worker = AnyEvent::Gearman::Worker->new(
    job_servers => [ 'localhost' ],
);

$worker->register_function('consume' => sub {
    my $job = shift;
    my $data = $job->workload;

    printf "worker: job.workload => %s\n", $data;

    my $result = 'thanks';

    $job->complete($result);
});

