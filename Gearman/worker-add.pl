use strict;
use warnings;

use Gearman::Worker;
use List::Util qw( sum );
use Storable qw( thaw );
use Try::Tiny;

my $worker = Gearman::Worker->new;
$worker->job_servers('localhost');

$worker->register_function('add' => sub {
    my $job = shift;

    print "ENTERING: add\n";

    my $arg = $job->arg;

    my $result;
    try {
        my $n = thaw($arg);
        $result = sum(@{ $n });
    }
    catch {
        print STDERR "ERROR: add: $_\n";
    };

    return $result;
});

for ( ;; ) {
    $worker->work(
    );
}


