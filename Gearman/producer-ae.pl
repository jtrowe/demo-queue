
use strict;
use warnings;

use AnyEvent::Gearman::Client;

my $client = AnyEvent::Gearman::Client->new(
    job_servers => [ 'localhost' ],
);

$client->add_task(
    consume => "Hello from $0",

    on_complete => sub {
        my $a = shift;
        my $result = shift;
        print "on_complete a.ref  = " . ref($a) . "\n";
        print "on_complete result = " . $result . "\n";
    },

);

