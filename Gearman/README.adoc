= Gearman

Demonstration of using Gearman.


== Running Gearman in Docker

[source,sh]
-----------
make run
-----------


== Stopping Gearman in Docker

[source,sh]
-----------
make stop
-----------


== Install libraries

[source,sh]
-----------
carton install
-----------


== Run `consumer-ae.pl`

[source,sh]
-----------
carton exec perl consumer-ae.pl
# CTRL-C to exit wait loop
-----------


== Run `producer.pl`

[source,sh]
-----------
carton exec perl producer.pl
-----------


== Links and References

*   http://gearman.org/
*   https://github.com/gearman/gearmand
*   https://github.com/artefactual-labs/docker-gearmand
*   http://gaspaio.github.io/gearmanui/
*   https://github.com/koryonik/gearman-ui.docker
*   https://metacpan.org/pod/AnyEvent::Gearman

