use strict;
use warnings;

use Beanstalk::Client;
use English qw( -no_match_vars );
use Log::Any::Adapter 'Stderr';

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);

my $client = Beanstalk::Client->new({
    server => 'localhost',
    tube   => 'default',
});
$client->debug(1);

my $job = $client->put({
    data => "Hello from $PROGRAM_NAME",
});

$log->info("job=$job");
$log->info("job.stats=" . $job->stats);

if ( my $error = $client->error ) {
    $log->info("error=$error");
}

