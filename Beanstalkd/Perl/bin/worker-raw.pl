use strict;
use warnings;

use Beanstalk::Client;
use English qw( -no_match_vars );
use Log::Any::Adapter 'Stderr';

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);

my $client = Beanstalk::Client->new({
    server => 'localhost',
    tube   => 'default',
});
$client->debug(1);

while ( my $job = $client->reserve ) {
    $log->info("job.data=" . $job->data);

    $job->delete;

    if ( my $error = $job->error ) {
        $log->error("error=$error");
    }
}

if ( my $error = $client->error ) {
    $log->error("error=$error");
}
