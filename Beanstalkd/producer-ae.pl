use strict;
use warnings;

use AnyEvent::Beanstalk;

my $client = AnyEvent::Beanstalk->new(
    server => 'localhost',
    debug  => 1,
);

my $job = $client->put({
    data => "Hello from $0",
})->recv;

my $stats = $client->stats->recv;

my @methods = qw( pid uptime total_jobs );
foreach my $m ( @methods ) {
    printf "client.stats.%-12s => %s\n", $m, $stats->$m;
}

