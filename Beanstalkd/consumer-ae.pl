use strict;
use warnings;

use AnyEvent::Beanstalk;

my $client = AnyEvent::Beanstalk->new(
    server => 'localhost',
    debug  => 1,
);

#$client->watch('default', sub {
$client->watch('default');
$client->reserve(sub {
    my $job = shift;

    print "job.data=" . $job->data . "\n";

    $job->delete;

    if ( my $error = $job->error ) {
        print "error=$error\n";
    }

});

