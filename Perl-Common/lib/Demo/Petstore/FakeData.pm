package Demo::Petstore::FakeData;


use Data::Fake qw( Core Names Text );
use Moo;


my $max_before = 90 * 24 * 60 * 60;
my $g_pet = fake_hash({
    id        => fake_int(1, 999),
    intake_at => sub {
        my $e = time - rand($max_before);
        return DateTime->from_epoch(epoch => $e);
    },
    name   => fake_name(),
    status => fake_pick('New', 'Adopted', 'Adoption Pending'),
    tag    => fake_words(1),
    type   => fake_pick('Cat', 'Dog', 'Guinea Pig'),
});
has generate_pet => (
    is      => 'ro',
    default => sub {
        return $g_pet;
    },
);


my $g_pets = fake_array(13, $g_pet);
has generate_pets => (
    is      => 'ro',
    default => sub {
        return $g_pets;
    },
);


1;
