package Demo::Petstore::Report::InventoryReport;


use DateTime;
use DateTime::Format::ISO8601;
use Demo::Petstore::FakeData;
use Log::Any;
use Moo;


has dt_format => (
    is      => 'ro',
    default => sub {
        return DateTime::Format::ISO8601->new,
    },
);


has log => (
    is       => 'ro',
    required => 1,
);


has raw_data => (
    is      => 'ro',
    default => sub {
        return Demo::Petstore::FakeData->new->generate_pets->();
    },
);


sub deflate {
    my $self  = shift;
    my %param = @_;
    my $rep   = $param{report};

    $rep->{meta}->{timestamp} .= '';
    foreach my $d ( @{ $rep->{data}->{pets} } ) {
        $d->{intake_at} .= '';
    }

    return $self;
}




sub generate {
    my $self  = shift;
    my %param = @_;
    my $date  = $param{date};

    $self->log->info('Generating InventoryReport');

    $date //= DateTime->now;

    my @raw = @{ $self->raw_data // [] };
    @raw = sort { $a->{id} <=> $b->{id} } @raw;

    my $fo = DateTime::Format::ISO8601->new;
    $date->set_formatter($fo);
    foreach my $r ( @raw ) {
        $r->{intake_at}->set_formatter($fo);
    }

    my $res = {
        meta => {
            timestamp => $date,
        },
        data => {
            count => scalar(@raw),
            pets  => \@raw,
        },
    };

    $self->log->info('Generated InventoryReport');

    return $res;
}


sub inflate {
    my $self  = shift;
    my %param = @_;
    my $rep   = $param{report};

    $rep->{meta}->{timestamp}
            = $self->dt_format->parse_datetime($rep->{meta}->{timestamp});
    foreach my $d ( @{ $rep->{data}->{pets} } ) {
        $d->{intake_at}
            = $self->dt_format->parse_datetime($d->{intake_at});
    }

    return $self;
}


sub inflate_params {
    my $self  = shift;
    my %param = @_;

    $param{date} = $self->dt_format->parse_datetime($param{date});

    return \%param;
}


1;
