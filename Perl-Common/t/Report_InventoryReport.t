use strict;
use warnings;

use Demo::Petstore::Report::InventoryReport;
use JSON;
use Log::Any::Adapter 'TAP', filter => 'none';

use Test::More;

my $j = JSON->new->canonical(1)->pretty(1);
my $l = Log::Any->get_logger(category => '');

my $gen = new_ok('Demo::Petstore::Report::InventoryReport', [ log => $l ]);
my $raw = $gen->raw_data;

my $param = $gen->inflate_params(date => '1970-01-01');
isa_ok($param->{date}, 'DateTime');


my $rep = $gen->generate;

isa_ok($rep->{meta}->{timestamp}, 'DateTime');

$gen->deflate(report => $rep);
is(ref $rep->{meta}->{timestamp}, '');

note("REPORT:\n" . $j->encode($rep));

$gen->inflate(report => $rep);
isa_ok($rep->{meta}->{timestamp}, 'DateTime');


done_testing;
