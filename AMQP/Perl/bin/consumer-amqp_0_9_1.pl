#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use English qw( -no_match_vars );
use Getopt::Long;
use Log::Any::Adapter 'Stderr';
use Net::AMQP::RabbitMQ;

my $channel       = 1;
my $exchange_name = 'amq.fanout';
my $route_key     = 'demo.route.one';
my $queue_name    = '';
my $user          = 'user';
my $password      = 'password';
GetOptions(
    'channel|c=i'    => \$channel,
    'exchange|e=s'   => \$exchange_name,
    'route_key|r=s'  => \$route_key,
    'queue_name|q=s' => \$queue_name,
);

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialized.');

my $mq = Net::AMQP::RabbitMQ->new;

my $hostname = 'localhost';
$log->info(sprintf 'Connecting to %s.', $hostname);
$mq->connect(
    $hostname,
    {
        user     => $user,
        password => $password,
    },
);

$log->info(sprintf 'Opening channel %d.', $channel);
$mq->channel_open($channel);

$log->info(sprintf 'Declaring queue "%s".', $queue_name);
$queue_name = $mq->queue_declare($channel, $queue_name);

$log->info(sprintf 'Binding queue "%s" to channel %d, route "%s" on exchange "%s".',
        $queue_name, $channel, $route_key, $exchange_name);
$mq->queue_bind($channel, $queue_name, $exchange_name, $route_key);

my @consume_args = (
    {
        no_ack => 0,
    },
);
$log->info(sprintf 'Consuming messages on channel %d.', $channel);
$mq->consume($channel, $queue_name, @consume_args);

my $quit         = 0;
my $recv_timeout = 300;

$SIG{INT} = sub {
    $quit = 1;
    $log->info('Got INT signal.');
};
$SIG{TERM} = sub {
    $quit = 1;
    $log->info('Got TERM signal.');
};

while ( ! $quit ) {
    my $message = $mq->recv($recv_timeout);
    if ( $message ) {
        $log->info("Received message:\n" . Dumper($message));

        my $delivery_tag = $message->{delivery_tag};
        $mq->ack($channel, $delivery_tag);
    }
}

$log->info('Disconnecting.');
$mq->disconnect;
