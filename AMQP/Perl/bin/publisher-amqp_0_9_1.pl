#!/usr/bin/env perl

use strict;
use warnings;

use English qw( -no_match_vars );
use Getopt::Long;
use Log::Any::Adapter 'Stderr';
use Net::AMQP::RabbitMQ;

my $channel       = 1;
my $exchange_name = 'amq.fanout';
my $route_key     = 'demo.route.one';
my $user          = 'user';
my $password      = 'password';
GetOptions(
    'channel|c=i'   => \$channel,
    'exchange|e=s'  => \$exchange_name,
    'route_key|r=s' => \$route_key,
);

my $log = Log::Any->get_logger(category => $PROGRAM_NAME);
$log->debug('Logging initialized.');

my $mq = Net::AMQP::RabbitMQ->new;

my $hostname = 'localhost';
$log->info(sprintf 'Connecting to %s.', $hostname);
$mq->connect(
    $hostname,
    {
        user     => $user,
        password => $password,
    },
);

$log->info(sprintf 'Opening channel %d.', $channel);
$mq->channel_open($channel);

$log->info(sprintf 'Publishing to channel %d, route "%s" on exchange "%s".',
        $channel, $route_key, $exchange_name);
$mq->publish(
    $channel,
    $route_key,
    'Message',
    {
        exchange => $exchange_name,
    },
);

sleep 50;

$log->info('Disconnecting.');
$mq->disconnect;
