= ActiveMQ

The ActiveMQ message broker.

Protocols enabled:

*   amqp 1.0
*   mqtt
*   openwire
*   stomp


== Start the server

[source,sh]
-----------
docker-compose up
-----------


== RabbitMQ Management Interface

Browse to http://0.0.0.0:8161/console .


== Links and References

*   https://activemq.apache.org/
*   https://hub.docker.com/r/apache/activemq-artemis

