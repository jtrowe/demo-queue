use strict;
use warnings;

use JSON;
use Kafka::Connection;
use Kafka::Producer;
use Scalar::Util qw( blessed );
use Try::Tiny;

try {
    # port appears to be necessary in my setup
    my $conn = Kafka::Connection->new(host => 'localhost', port => 29092);
    my $prod = Kafka::Producer->new(Connection => $conn);

    $prod->send(
        'mytopic',
        0,
        JSON->new->utf8->pretty(1)->encode({
            foo       => 'bar',
            id        => 42,
            timestamp => time(),
            blurb     => 'mary had a little lamb',
        }),
    );

}
catch {
    my $error = $_;

    if ( blessed($error) && $error->isa('Kafka::Exception') ) {
        warn sprintf 'ERROR : (%s) %s', $error->code, $error->message;
    }
};

