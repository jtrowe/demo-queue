use strict;
use warnings;

use Data::Printer;
use JSON;
use Kafka;
use Kafka::Connection;
use Kafka::Consumer;
use Scalar::Util qw( blessed );
use Try::Tiny;

my $json_decoder = JSON->new->utf8;

try {
    # port appears to be necessary in my setup
    my $conn = Kafka::Connection->new(host => 'localhost', port => 29092);
    my $cons = Kafka::Consumer->new(Connection => $conn);

    my $messages = $cons->fetch('mytopic', 0, 0);
    my @messages = @{ $messages };
    printf "messages.count=%d\n" . scalar(@messages);
    foreach my $m ( @messages ) {
        printf "message.payload:\n%s\n", dec($m->payload);
        print "\n";
    }

}
catch {
    my $error = $_;

    if ( blessed($error) && $error->isa('Kafka::Exception') ) {
        warn sprintf 'ERROR : (%s) %s', $error->code, $error->message;
    }
};

sub dec {
    my $payload = shift;

    my $result;
    try {
        $result = np($json_decoder->decode($payload));
    }
    catch {
        warn "$_";
        $result = $payload;
    };

    return $result;
}

