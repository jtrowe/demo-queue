use strict;
use warnings;

use lib 'lib';

use Demo::Queue::Minion::Math;

use Test::More;

is(0, Demo::Queue::Minion::Math::fib(1), 'F(1) correct');
is(1, Demo::Queue::Minion::Math::fib(2), 'F(1) correct');
is(1, Demo::Queue::Minion::Math::fib(3), 'F(1) correct');
is(2, Demo::Queue::Minion::Math::fib(4), 'F(1) correct');

done_testing;
