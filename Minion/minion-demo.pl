#!/usr/bin/env perl

use Mojolicious::Lite -signatures;

use lib 'lib';

use Demo::Queue::Minion::Math;

plugin Minion => {
  SQLite => 'sqlite:minion.db',
};

plugin 'Minion::Admin';

# Slow task
app->minion->add_task(count_slow => sub ($job, @args) {
  my $max = 20;
  for ( my $i = 0; $i < $max; $i++ ) {
    $job->app->log->debug("count_slow i=$i");
    sleep 1;
  }
});

app->minion->add_task(sum_slow => sub ($job, @args) {
  my $n = 'sum_slow';

  my $sum = 0;
  foreach my $a ( @args ) {
    $job->app->log->debug("$n: a=$a");
    $sum += $a;
  }

  my $progress = 0;
  $job->note(progress => $progress);

  my $max = 20;
  for ( my $i = 0; $i < $max; $i++ ) {
    $progress = $i / $max;
    $job->note(progress => $progress);
    $job->app->log->debug("$n: i=$i");
    sleep 1;
  }

  $progress = 1;
  $job->note(progress => $progress);
  $job->finish({
    sum => $sum,
  });
});

app->minion->add_task(fib_slow => sub ($job, @args) {
  my $na = 'fib_slow';

  my @v = ( 0 );

  my $progress = 0;
  $job->note(progress => $progress);
  # FIXME find way to clear result on re-run
#  $job->result(result => undef);

  my $n = shift @args;

  my $i = 1;
  while ( @v <= $n ) {
    my $v = Demo::Queue::Minion::Math::fib($i);

    $v[$i] = $v;

    $job->app->log->debug("$na F($n): loop: F($i)=$v[$i]");

    $progress = $i / $n;
    $job->note(progress => $progress);

    $i++;

    sleep 1;
  }

  $progress = 1;
  $job->note(progress => $progress);
  $job->finish({
    result => $v[$n],
  });
});

get '/' => sub ($c) {
  $c->minion->enqueue('count_slow');
  $c->minion->enqueue('sum_slow', [ 1, 2, 3 ]);
  $c->minion->enqueue('fib_slow', [ 5 ]);
  $c->render(template => 'index');
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
