package Demo::Queue::Minion::Math;

use strict;
use warnings;

sub fib {
    my $n = shift || 0;

    if ( 0 == $n ) {
        die "fib(0) is undef";
    }
    elsif ( 1 == $n ) {
        return 0;
    }
    elsif ( 2 == $n ) {
        return 1;
    }

    return fib($n - 1) + fib($n - 2);
}

1;
